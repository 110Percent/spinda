import Spinda from "./class/Spinda";
import { readFileSync } from "fs";
import { join } from "path";

const config = JSON.parse(
  readFileSync(join(__dirname, "../config/config.json"), "utf-8")
);
const spinda = new Spinda(config);

spinda.discord.on("ready", () => {
  console.log(`Ready! Logged in as ${spinda.discord.user?.tag}`);
});

export default spinda.discord;
