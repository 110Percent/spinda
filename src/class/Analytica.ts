import Spinda from "./Spinda";
import { Message } from "discord.js";
import { Model, ModelCtor } from "sequelize/types";

class Analytica {
  spinda: Spinda;
  db: ModelCtor<Model<any, any>>;

  constructor(spinda: Spinda) {
    this.spinda = spinda;
    this.spinda.db.import("../models/analytics.js");
    this.spinda.db.sync();
    this.db = this.spinda.db.models["Analytics"];
  }

  async logPokemon(msg: Message, name: string): Promise<null> {
    if (msg.guild) {
      const gRecord: any = (
        await this.db.findOrCreate({
          where: { name, identifier: msg.guild.id, type: "GUILD" },
          defaults: { hits: 0 },
        })
      )[0];
      await gRecord.update({ hits: gRecord.hits + 1 });
    }

    const uRecord: any = (
      await this.db.findOrCreate({
        where: { name, identifier: msg.author.id, type: "USER" },
        defaults: { hits: 0 },
      })
    )[0];
    await uRecord.update({ hits: uRecord.hits + 1 });

    return null;
  }

  async fetch(
    where: { identifier: string; type: string },
    limit: number
  ): Promise<any[]> {
    const records = await this.db.findAll({
      where,
      limit,
      raw: true,
      order: [["hits", "DESC"]],
    });
    return records;
  }
}

export default Analytica;
