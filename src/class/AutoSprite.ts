import request from "superagent";
import Spinda from "./Spinda";
import { Message } from "discord.js";

class AutoSprite {
  spinda: Spinda;
  baseURL: string;
  spriteFiles: Set<string>;
  spriteBases: { [key: string]: string };
  parseFilters: { [key: string]: string };

  constructor(spinda: Spinda) {
    this.spinda = spinda;
    this.baseURL = "https://play.pokemonshowdown.com/sprites";

    this.spriteFiles = new Set();
    request.get(`${this.baseURL}/ani`).then((res) => {
      const gifRegex = /href="([a-z0-9-]+)\.gif"/g;
      let matches: RegExpExecArray | null;
      while ((matches = gifRegex.exec(res.text))) {
        this.spriteFiles.add(matches[1]);
      }
    });

    this.spriteBases = {
      bw: "bw",
      gen1: "gen1",
      gen2: "gen2",
      gen3: "gen3",
      gen4: "gen4",
      gen5: "gen5",
      gen5ani: "gen5ani",
      bwani: "gen5ani",
      back: "ani-back",
      "bw-ani": "gen5ani",
      hgss: "gen4",
      dp: "gen4dp",
      rs: "gen3",
      frlg: "gen3frlg",
      crystal: "gen2",
      gold: "gen2g",
      silver: "gen2s",
      yellow: "gen1",
      rb: "gen1rb",
      rg: "gen1rg",
    };

    this.parseFilters = {
      "mega-(.*)-x": "&-megax",
      "mega-(.*)-y": "&-megay",
      "mega-(.*)$": "&-mega",
      "gmax-(.*)-x": "&-gmax",
      "gigantamax-(.*)": "&-gmax",
      "alolan-(.*)": "&-alola",
      "galarian-(.*)": "&-galar",
      "female-(.*)": "&-f",
      "(.*)-female": "&-f",
    };
  }

  async parse(
    msg: Message,
    splitter: string,
    ignore?: any[]
  ): Promise<Sprite[]> {
    const content = msg.content;
    const split = content.split(splitter);
    if (split.length < 2) {
      return [];
    }
    const toReturn: any[] = [];
    for (let i = 1; i < split.length; i++) {
      const entry = split[i];
      let formatted: Sprite | null;
      if (msg.guild) {
        formatted = await this.formatString(entry, msg.guild.id);
      } else {
        formatted = await this.formatString(entry);
      }
      if (
        formatted !== null &&
        this.spriteFiles.has(formatted.name) &&
        !(
          ignore &&
          ignore.some((i) => !i || this.sameSprites(i, <Sprite>formatted))
        ) &&
        !(
          toReturn.length > 0 &&
          toReturn.some((e) => !e || this.sameSprites(e, <Sprite>formatted))
        ) &&
        (ignore ? ignore.length + toReturn.length < 4 : toReturn.length < 4)
      ) {
        toReturn.push(formatted);
      }
    }
    return toReturn;
  }

  async formatString(input: string, guildID?: string): Promise<Sprite | null> {
    if (input.length < 3) return null;
    if (guildID) {
      input = await this.spinda.aliasManager.check(input, guildID);
    }
    let filtered = input.toLowerCase().replace(/ /g, "-");
    const toReturn = { name: filtered, shiny: false, back: false, base: "ani" };
    const split = filtered.split("-");
    if (split.includes("shiny")) {
      split.splice(split.indexOf("shiny"), 1);
      toReturn.shiny = true;
    }
    if (split.includes("back")) {
      split.splice(split.indexOf("back"), 1);
      toReturn.back = true;
    }
    for (const base of Object.keys(this.spriteBases)) {
      if (split.includes(base)) {
        split.splice(split.indexOf(base), 1);
        toReturn.base = this.spriteBases[base];
      }
    }
    filtered = split.join("-");
    for (const key of Object.keys(this.parseFilters)) {
      const match = filtered.match(new RegExp(key));
      if (match) {
        filtered = filtered.replace(
          match[0],
          this.parseFilters[key].replace("&", match[1])
        );
      }
    }
    if (filtered === "prandom") {
      filtered = (<any>Array.from(this.spriteFiles))[
        Math.floor(Math.random() * this.spriteFiles.size)
      ];
    }
    toReturn.name = filtered;
    return toReturn;
  }

  sameSprites(first: Sprite, second: Sprite): boolean {
    return (
      first.name === second.name &&
      first.shiny === second.shiny &&
      first.back === second.back &&
      first.base === second.base
    );
  }
}

export default AutoSprite;
