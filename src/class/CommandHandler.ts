import { Message } from "discord.js";
import Command from "./Command";
import { join } from "path";
import { readdirSync } from "fs";
import Spinda from "./Spinda";

class CommandHandler {
  commands: { [key: string]: Command };
  spinda: Spinda;

  constructor(spinda: Spinda) {
    this.commands = {};
    this.spinda = spinda;
    this.scan("commands");
  }

  scan(dName: string): void {
    const dir = join(__dirname, "../", dName);
    const files = readdirSync(dir);
    for (const file of files) {
      if (!file.endsWith(".js")) continue;
      const name = file.split(".")[0];
      this.commands[name] = new (require(`../commands/${file}`).default)(
        this.spinda
      );
    }
  }

  async handle(msg: Message, prefix: string): Promise<void> {
    const split = msg.content.split(" ");
    const cmdName = split[0].substring(prefix.length).toLowerCase();
    const cmd = this.getCommand(cmdName);
    if (!cmd) return;
    if (cmd.opts.guildRequired && !msg.guild) {
      msg.channel.send(
        "Sorry, this command can't be used in a private message."
      );
      return;
    }
    if (cmd.opts.perms) {
      for (const perm of cmd.opts.perms) {
        if (!msg.member?.hasPermission(perm)) {
          msg.channel.send(
            `Sorry, the \`${perm}\` perission is required to use this command.`
          );
          return;
        }
      }
    }
    const args = split.length > 1 ? split.slice(1).join(" ") : "";
    await cmd.run(msg, args);
  }

  getCommand(name: string): Command | null {
    let cmd = this.commands[name];
    if (cmd === undefined) {
      const match = Object.values(this.commands).filter((c) => {
        return c.opts.aliases && c.opts.aliases.indexOf(name) > -1;
      });
      if (match.length < 1) {
        return null;
      }
      cmd = match[0];
    }
    return cmd;
  }
}

export default CommandHandler;
