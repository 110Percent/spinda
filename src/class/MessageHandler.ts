import { Message, MessageEmbed } from "discord.js";
import Spinda from "./Spinda";
import CommandHandler from "./CommandHandler";
import AutoSprite from "./AutoSprite";

class MessageHandler {
  spinda: Spinda;
  cmdHandler: CommandHandler;
  autoSprite: AutoSprite;
  splitters: string[];

  constructor(spin: Spinda) {
    this.spinda = spin;
    this.cmdHandler = new CommandHandler(this.spinda);
    this.autoSprite = new AutoSprite(this.spinda);
    this.splitters = ["*", "_"];

    this.spinda.discord.on("message", this.handle.bind(this));
  }

  async handle(msg: Message): Promise<void> {
    if (msg.author.bot) {
      return;
    }
    if (msg.content.startsWith(this.spinda.config.bot.defaultPrefix)) {
      await this.cmdHandler.handle(msg, this.spinda.config.bot.defaultPrefix);
    } else if (/[*_]/.test(msg.content)) {
      let sprites: Sprite[] = [];
      for (const splitter of this.splitters) {
        if (msg.content.includes(splitter)) {
          sprites = [
            ...sprites,
            ...(await this.autoSprite.parse(msg, splitter)),
          ];
        }
      }
      for (const sprite of sprites) {
        const extension = sprite.base.includes("ani") ? "gif" : "png";
        const modifier = `${sprite.back ? "-back" : ""}${
          sprite.shiny ? "-shiny" : ""
        }`;
        const spriteURL = `${this.autoSprite.baseURL}/${sprite.base}${modifier}/${sprite.name}.${extension}`;
        msg.channel.send({
          embed: new MessageEmbed({ image: { url: spriteURL } }),
        });
        this.spinda.analytica.logPokemon(msg, sprite.name);
      }
    }
  }
}

export default MessageHandler;
