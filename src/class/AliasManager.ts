import Spinda from "./Spinda";
import { Model, ModelCtor } from "sequelize/types";

class AliasManager {
  spinda: Spinda;
  table: ModelCtor<Model<any, any>>;

  constructor(spinda: Spinda) {
    this.spinda = spinda;
    this.spinda.db.import("../models/alias.js");
    this.spinda.db.sync();
    this.table = this.spinda.db.models["Aliases"];
  }

  async set(key: string, value: string, guild: string): Promise<void> {
    const entry = await this.table.findOne({
      where: { key, guild },
    });
    if (entry) {
      await entry.update({ value });
    } else {
      await this.table.create({ key, value, guild });
    }
  }

  async list(guild: string): Promise<any[]> {
    const entries = await this.table.findAll({
      where: { guild },
      raw: true,
    });

    return entries;
  }

  async check(key: string, guild: string): Promise<string> {
    const entry: any = await this.table.findOne({
      where: { key, guild },
      raw: true,
    });
    if (entry) {
      return entry.value;
    } else {
      return key;
    }
  }

  async delete(key: string, guild: string): Promise<void> {
    await this.table.destroy({
      where: { key, guild },
    });
  }
}

export default AliasManager;
