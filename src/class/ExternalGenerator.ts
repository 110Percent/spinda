class ExternalGenerator {
  endpoints: {
    [key: string]: { urls: { [key: string]: string }; modifiers: string[] };
  };
  modifiers: { [key: string]: any };

  constructor() {
    this.endpoints = {
      bulbapedia: {
        urls: {
          pokemon: "https://bulbapedia.bulbagarden.net/wiki/$",
          item: "https://bulbapedia.bulbagarden.net/wiki/$",
          ability: "https://bulbapedia.bulbagarden.net/wiki/$__(Ability\\)",
          move: "https://bulbapedia.bulbagarden.net/wiki/$__(move\\)",
        },
        modifiers: ["camel", "underscore"],
      },
      pokemondb: {
        urls: {
          pokemon: "https://pokemondb.net/pokedex/$",
          item: "https://pokemondb.net/item/$",
          ability: "https://pokemondb.net/ability/$",
          move: "https://pokemondb.net/move/$",
        },
        modifiers: ["stripMega", "stripRegion", "hyphen"],
      },
      smogon: {
        urls: {
          pokemon: "https://www.smogon.com/dex/ss/pokemon/$",
          item: "https://www.smogon.com/dex/ss/items/$",
          ability: "https://www.smogon.com/dex/ss/abilities/$",
          move: "https://www.smogon.com/dex/ss/moves/$",
        },
        modifiers: ["stripMega", "hyphen"],
      },
      serebii: {
        urls: {
          pokemon: "https://www.serebii.net/pokedex-sm/$.shtml",
          item: "https://www.serebii.net/itemdex/$.shtml",
          ability: "https://www.serebii.net/abilitydex/$.shtml",
          move: "https://www.serebii.net/attackdex-swsh/$.shtml",
        },
        modifiers: ["padNum", "noSpace"],
      },
    };
    this.modifiers = {
      camel: (s: string) =>
        s
          .split(" ")
          .map((w: string) => w.charAt(0).toUpperCase() + w.slice(1))
          .join(" "),
      underscore: (s: string) => s.replace(/ /g, "_"),
      hyphen: (s: string) => s.replace(/ /g, "-"),
      noSpace: (s: string) => s.replace(/ /g, ""),
      stripMega: (s: string) =>
        s
          .split(" ")
          .filter((w) => !["mega", "x", "y"].includes(w))
          .join(" "),
      stripRegion: (s: string) =>
        s
          .split(" ")
          .filter((w) => !["alolan", "galarian"].includes(w))
          .join(" "),
      padNum: (s: string) => s.padStart(3, "0"),
    };
  }

  generate(name: string, type: string, ep: string): string | undefined {
    const endpoint = this.endpoints[ep];
    let query = name.toString().toLowerCase();
    if (!endpoint) return;
    for (const mod of endpoint.modifiers) {
      query = this.modifiers[mod](query);
    }
    return endpoint.urls[type].replace("$", query);
  }
}

export default ExternalGenerator;
