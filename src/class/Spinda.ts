import { Client } from 'discord.js';
import { Sequelize } from 'sequelize'
import MessageHandler from './MessageHandler';
import { readdirSync } from 'fs';
import { join } from 'path';
import AliasManager from './AliasManager';
import Analytica from './Analytica';

class Spinda {
  aliasManager: AliasManager;
  analytica: Analytica;
  discord: Client;
  db: Sequelize;
  minccino: Sequelize;
  messageHandler: MessageHandler;
  config: SpindaOptions;

  constructor(options: SpindaOptions) {
    this.config = options;
    this.discord = new Client();
    this.db = new Sequelize(Object.assign(options.db, { database: 'spinda' }));
    this.minccino = new Sequelize(Object.assign(options.db, { database: 'minccino' }));
    this.aliasManager = new AliasManager(this);

    for (const model of readdirSync(join(__dirname, '../models/minccino'))) {
      this.minccino.import(join(__dirname, '../models/minccino', model));
    }

    this.db.authenticate();
    this.minccino.authenticate();
    this.discord.login(options.discord.token);
    this.messageHandler = new MessageHandler(this);
    this.analytica = new Analytica(this);
  }
}

export default Spinda;