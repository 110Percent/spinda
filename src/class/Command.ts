import { Message } from "discord.js";
import Spinda from "./Spinda";
import CommandOptions from "../types/CommandOptions";

class Command {
  name: string;
  opts: CommandOptions;
  spinda: Spinda;

  constructor(name: string, opts: CommandOptions, spinda: Spinda) {
    this.name = name;
    this.opts = opts;
    this.spinda = spinda;
  }

  async run(ctx: Message, args?: string): Promise<void> {
    console.log(`No action configured for command ${this.name}.`);
  }
}

export default Command;
