interface SpindaOptions {
  discord: {
    token: string
  },
  db: {
    host: string,
    port: number,
    username: string,
    password: string,
    dialect: 'mysql'
  },
  bot: {
    defaultPrefix: string,
    owners: string[]
  }
};