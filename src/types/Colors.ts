enum Colors {
    RED = 11224642,
    BLUE = 8171458,
    YELLOW = 16239240,
    GREEN = 10597740,
    BLACK = 5789784,
    BROWN = 10578246,
    PURPLE = 12225455,
    GRAY = 12105912,
    WHITE = 14211288,
    PINK = 16289992
}

enum TypeColors {
    Bug = 11057184,
    Dark = 7362632,
    Dragon = 7354616,
    Electric = 16306224,
    Fairy = 15636908,
    Fighting = 12595240,
    Fire = 15761456,
    Flying = 11047152,
    Ghost = 7362712,
    Grass = 7915600,
    Ground = 14729320,
    Ice = 10016984,
    Normal = 11053176,
    Poison = 10502304,
    Psychic = 16275592,
    Rock = 12099640,
    Steel = 12105936,
    Water = 6852848
}

export { Colors, TypeColors };