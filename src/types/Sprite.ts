interface Sprite {
    name: string;
    shiny: boolean;
    back: boolean;
    base: string;
}