import { PermissionString } from "discord.js";

interface CommandOptions {
    aliases?: string[];
    desc?: string;
    category?: string;
    perms?: PermissionString[];
    usage?: string[];
    hidden?: boolean;
    guildRequired?: boolean;
}

export default CommandOptions;