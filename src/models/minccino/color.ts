import { Sequelize } from "sequelize/types";
import path from 'path';

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

	const Color = sequelize.define('Colors', {
		name: { type: DataTypes.STRING(16) }
	});

	return Color;
}