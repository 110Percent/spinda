import { Sequelize } from "sequelize/types";

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

    const Analytics = sequelize.define('Analytics', {
        name: { type: DataTypes.STRING },
        hits: { type: DataTypes.INTEGER },
        identifier: { type: DataTypes.STRING },
        type: { type: DataTypes.STRING }
    }, {
        freezeTableName: true
    });

    return Analytics;
}