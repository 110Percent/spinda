import { Sequelize } from "sequelize/types";

export default function (sequelize: Sequelize, DataTypes: { [key: string]: any }) {

    const Alias = sequelize.define('Aliases', {
        key: { type: DataTypes.STRING },
        value: { type: DataTypes.STRING },
        guild: { type: DataTypes.STRING(20) }
    }, {
        freezeTableName: true
    });

    return Alias;
}