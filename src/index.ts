import { ShardingManager, Shard } from 'discord.js';
import { readFileSync } from 'fs';
import { join } from 'path';

const config = JSON.parse(readFileSync(join(__dirname, '../config/config.json'), 'utf-8'));

const manager = new ShardingManager('./dist/spinda.js', {
  token: config.discord.token
});

manager.spawn();

manager.on('shardCreate', (shard: Shard) => console.log(`Shard #${shard.id} launched`));