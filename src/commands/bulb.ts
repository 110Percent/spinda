import Command from "../class/Command";
import { Message } from "discord.js";
import Spinda from "../class/Spinda";
import request from "superagent";

class BulbCommand extends Command {
  baseUrl: string;

  constructor(spinda: Spinda) {
    super(
      "bulb",
      {
        desc: "Searches for a page on Bulbapedia",
        category: "External",
        usage: ["<query>"],
      },
      spinda
    );
    this.baseUrl =
      "https://bulbapedia.bulbagarden.net/w/api.php?action=opensearch&format=json&formatversion=2&namespace=0&limit=1&suggest=true&search=";
  }

  async run(ctx: Message, args: string): Promise<void> {
    const encoded = encodeURIComponent(args);
    const res = await request.get(this.baseUrl + encoded);
    if (res.body[1].length > 0) {
      ctx.channel.send(
        `🔗 **${
          res.body[1][0]
        }** ${ctx.author.toString()}\n${res.body[3][0].replace(
          "http",
          "https"
        )}`
      );
    } else {
      ctx.channel.send("Query not found.");
    }
  }
}

export default BulbCommand;
