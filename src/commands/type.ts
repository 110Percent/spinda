import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import sequelize, { Model } from "sequelize";
import { TypeColors } from "../types/Colors";

class TypeCommand extends Command {
  typeNames: string[];
  damageCategories: string[][];

  constructor(spinda: Spinda) {
    super(
      "type",
      {
        desc: "Shows damage modifiers for one or more types.",
        category: "Pokémon",
        usage: ["<type> [type] ...", "<move>", "<pokemon>"],
      },
      spinda
    );
    this.typeNames = [];
    this.spinda.minccino.models["PokeTypes"]
      .findAll({ raw: true })
      .then((types: any) => {
        this.typeNames = types.map((t: {name: string}) => t.name);
      });
    this.damageCategories = [
      [
        "Supereffective against",
        "Deals normal damage to",
        "Resisted by",
        "Does not affect",
      ],
      ["Weak to", "Takes normal damage from", "Resists", "Immune to"],
    ];
  }

  async run(ctx: Message, args: string): Promise<void> {
    const search = args.toLowerCase();
    let split = args.toLowerCase().split(" ");
    const doAspect = [true, true];
    let titlePrefix = "";
    if (
      this.typeNames.map((t: string) => t.toLowerCase()).indexOf(search) < 0
    ) {
      let res: any[] = await this.spinda.minccino.models["Moves"].findAll({
        where: sequelize.where(
          sequelize.fn("lower", sequelize.col("name")),
          search
        ),
        raw: true,
      });
      if (res.length > 0) {
        split = [this.typeNames[res[0].move_type - 1].toLowerCase()];
        titlePrefix = res[0].name;
        doAspect[1] = false;
      } else {
        res = await this.spinda.minccino.models["Pokemon"].findAll({
          where: sequelize.where(
            sequelize.fn("lower", sequelize.col("name")),
            search
          ),
          raw: true,
        });
        if (res.length > 0) {
          split = [this.typeNames[res[0].type1 - 1].toLowerCase()];
          if (res[0].type2) {
            split.push(this.typeNames[res[0].type2 - 1].toLowerCase());
          }
          titlePrefix = res[0].name;
          doAspect[0] = false;
        }
      }
    }
    const multipliers: { atk: number[]; def: number[] } = { atk: [], def: [] };
    const selTypes: string[] = [];
    for (const _t of this.typeNames) {
      if (doAspect[0]) {
        multipliers.atk.push(1.0);
      }
      if (doAspect[1]) {
        multipliers.def.push(1.0);
      }
    }
    for (const name of split) {
      const tIndex = this.typeNames.map((t) => t.toLowerCase()).indexOf(name);
      if (tIndex < 0) {
        continue;
      }
      selTypes.push(this.typeNames[tIndex]);
      if (doAspect[0]) {
        const atkModifiers = await this.fetchModifiers(tIndex + 1, true);
        for (const m of <any[]>atkModifiers) {
          multipliers.atk[m.def - 1] *= Number(m.multiplier);
        }
      }
      if (doAspect[1]) {
        const defModifiers = await this.fetchModifiers(tIndex + 1, false);
        for (const m of <any[]>defModifiers) {
          multipliers.def[m.atk - 1] *= Number(m.multiplier);
        }
      }
    }

    if (selTypes.length < 1) {
      ctx.channel.send(
        "Not found. Your query must be a move, Pok\u00e9mon or a space-separated list of types."
      );
      return;
    }

    const stringLists: string[][][] = [[], []];
    if (doAspect[0]) {
      stringLists[0] = this.buildStrings(multipliers.atk);
    }
    if (doAspect[1]) {
      stringLists[1] = this.buildStrings(multipliers.def);
    }

    const embed = new MessageEmbed()
      .setTitle(
        titlePrefix
          ? `${titlePrefix} (${selTypes.join(", ")})`
          : selTypes.join(", ")
      )
      .setColor((<any>TypeColors)[selTypes[0]]);

    if (doAspect[0]) {
      embed.addField(
        "Offense",
        stringLists[0]
          .map((l: string[]) => l.join(", "))
          .map((s: string, i: number) => {
            return s.length > 0
              ? `*${this.damageCategories[0][i]}:*\n${s}\n\n`
              : "";
          })
          .join("")
      );
    }
    if (doAspect[1]) {
      embed.addField(
        "Defense",
        stringLists[1]
          .map((l: string[]) => l.join(", "))
          .map((s: string, i: number) => {
            return s.length > 0
              ? `*${this.damageCategories[1][i]}:*\n${s}\n\n`
              : "";
          })
          .join("")
      );
    }

    ctx.channel.send({ embed });
  }

  async fetchModifiers(id: number, atk: boolean): Promise<Model<unknown, unknown>[]> {
    const where: any = atk ? { atk: id } : { def: id };
    return await this.spinda.minccino.models["TypeDamage"].findAll({
      where,
      raw: true,
    });
  }

  buildStrings(multipliers: number[]): string[][] {
    const typeStrings: string[][] = [[], [], [], []];
    for (let i = 0; i < multipliers.length; i++) {
      const multi = multipliers[i];
      let strIndex = 1;
      if (multi > 1) {
        strIndex = 0;
      } else if (multi === 0) {
        strIndex = 3;
      } else if (multi < 1) {
        strIndex = 2;
      }
      typeStrings[strIndex].push(
        `${this.typeNames[i]}${
          multi === 0 || multi === 1 ? "" : " (x" + multi + ")"
        }`
      );
    }
    return typeStrings;
  }
}

export default TypeCommand;
