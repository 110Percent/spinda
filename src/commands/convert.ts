import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import Qty from "js-quantities";

class ConvertCommand extends Command {
  constructor(spinda: Spinda) {
    super(
      "convert",
      {
        desc: "Converts a measurement between two units.",
        category: "Utility",
        usage: ["<number> <unit> to <unit>"],
      },
      spinda
    );
  }

  async run(ctx: Message, args: string): Promise<void> {
    const split = args.split(" to ");
    if (split.length < 1) {
      return;
    }

    const base = Qty.parse(split[0]);
    if (!base) {
      ctx.channel.send(
        "Base unit not found. Check your spelling and try again."
      );
      return;
    }

    let converted: Qty;
    try {
      converted = base.to(split[1]);
    } catch (err) {
      ctx.channel.send(
        "Target unit not found or not compatible. Check your spelling and try again."
      );
      return;
    }
    converted = converted.toPrec(0.01);

    const embed = new MessageEmbed()
      .setTitle("Unit Conversion")
      .addField("Input", base.toString(), true)
      .addField("Output", converted.toString(), true);

    ctx.channel.send(embed);
  }
}

export default ConvertCommand;
