import Command from "../class/Command";
import { Message } from "discord.js";
import Spinda from "../class/Spinda";

class PingCommand extends Command {
  responses: string[];

  constructor(spinda: Spinda) {
    super(
      "ping",
      {
        desc: "Checks Spinda's response time.",
        category: "Utility",
        aliases: ["pong"],
      },
      spinda
    );
    this.responses = [
      "Pong!",
      "Spinda!",
      "Remember to drink water!",
      "Wash your hands!",
      "Don't touch your face!",
      "SPEEN!",
      "*drr drr drr*",
      "Now with extra things!",
      "doot doot",
      "Now with blockchain!",
      "spinda is love, spinda is life",
    ];
  }

  async run(ctx: Message): Promise<void> {
    const res = await ctx.channel.send(
      this.responses[Math.floor(Math.random() * this.responses.length)]
    );
    res.edit(
      `Response time: ${res.createdTimestamp - ctx.createdTimestamp} ms`
    );
  }
}

export default PingCommand;
