import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";

class HelpCommand extends Command {
  constructor(spinda: Spinda) {
    super(
      "help",
      {
        desc: "Shows a list of commands or usage for a specific command.",
        usage: ["[command]"],
      },
      spinda
    );
  }

  async run(ctx: Message, args?: string): Promise<void> {
    const query = args?.toLowerCase() || null;
    const cmds: { [key: string]: Command } = this.spinda.messageHandler
      .cmdHandler.commands;

    let embed: MessageEmbed;
    if (
      query &&
      this.spinda.messageHandler.cmdHandler.getCommand(query) &&
      !this.spinda.messageHandler.cmdHandler.getCommand(query)?.opts.hidden
    ) {
      embed = this.cmdEmbed(
        <Command>this.spinda.messageHandler.cmdHandler.getCommand(query)
      );
    } else {
      embed = this.defaultEmbed(cmds);
    }

    ctx.channel.send({ embed });
  }

  defaultEmbed(cmds: { [key: string]: Command }): MessageEmbed {
    const categories: { [key: string]: string[] } = { "\u200b": [] };
    for (const name in cmds) {
      const cmd = cmds[name];
      if (cmd.opts.hidden) {
        continue;
      }
      const cmdString = `\`${this.spinda.config.bot.defaultPrefix}${
        cmd.name
      }\`${cmd.opts.desc ? " - " + cmd.opts.desc : ""}`;
      if (!cmd.opts.category) {
        categories["\u200b"].push(cmdString);
      } else if (categories[cmd.opts.category]) {
        categories[cmd.opts.category].push(cmdString);
      } else {
        categories[cmd.opts.category] = [cmdString];
      }
    }
    const embed = new MessageEmbed().setTitle("Spinda Commands");
    for (const cat in categories) {
      embed.addField(cat, categories[cat].join("\n"));
    }
    return embed;
  }

  cmdEmbed(cmd: Command): MessageEmbed {
    const embed = new MessageEmbed()
      .setTitle(this.spinda.config.bot.defaultPrefix + cmd.name)
      .setDescription(cmd.opts.desc);

    if (cmd.opts.usage) {
      embed.addField(
        "Usage",
        cmd.opts.usage
          .map(
            (u) => `\`${this.spinda.config.bot.defaultPrefix}${cmd.name} ${u}\``
          )
          .join("\n")
      );
    }

    if (cmd.opts.aliases) {
      embed.addField(
        "Aliases",
        cmd.opts.aliases
          .map((c) => `\`${this.spinda.config.bot.defaultPrefix}${c}\``)
          .join(", ")
      );
    }

    return embed;
  }
}

export default HelpCommand;
