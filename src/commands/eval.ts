import Command from "../class/Command";
import { Message } from "discord.js";
import Spinda from "../class/Spinda";

class EvalCommand extends Command {
  constructor(spinda: Spinda) {
    super(
      "eval",
      {
        desc: "Used to evaulate JavaScript on Spinda's machine.",
        category: "Utility",
        hidden: true,
      },
      spinda
    );
  }

  async run(ctx: Message, args: string): Promise<void> {
    if (!this.spinda.config.bot.owners.includes(ctx.author.id)) {
      return;
    }
    const split = args.split("\n").filter((s) => s !== "");
    if (!split[1].startsWith("```")) {
      return;
    }
    const code = split.slice(2, split.length - 1).join("\n");
    try {
      let evalled: any;
      if (split[0] === "ctx") {
        evalled = eval(code);
      } else if (split[0] === "shard") {
        evalled = (() => {
          return eval(code);
        }).call(this.spinda);
      } else if (split[0] === "all") {
        evalled = await this.spinda.discord.shard?.broadcastEval(code);
      }
      ctx.channel.send(`\`\`\`js\n${evalled}\n\`\`\``);
    } catch (err) {
      ctx.channel.send(`**Error**\n\`\`\`js\n${err}\n\`\`\``);
    }
  }
}

export default EvalCommand;
