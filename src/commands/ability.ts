import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import { QueryTypes } from "sequelize";
import ExternalGenerator from "../class/ExternalGenerator";

class AbilityCommand extends Command {
  ratings: { [key: number]: string };
  external: ExternalGenerator;

  constructor(spinda: Spinda) {
    super(
      "ability",
      {
        desc: "Shows information on an ability.",
        category: "Pokémon",
        usage: ["<ability>"],
      },
      spinda
    );
    this.ratings = {
      "-2": "Very Detrimental",
      "-1": "Detrimental",
      0: "Useless",
      1: "Inffective",
      2: "Circumstancial",
      3: "Useful",
      4: "Very Useful",
      5: "Essential",
    };
    this.external = new ExternalGenerator();
  }

  async run(ctx: Message, args: string): Promise<void> {
    const toFetch = args.replace(/[^a-zA-Z0-9]/g, "");

    if (toFetch.length < 1) {
      ctx.channel.send("Ability not found. Check your spelling and try again.");
      return;
    }

    const res = await this.spinda.minccino.query(
      `SELECT * FROM \`Abilities\` where SOUNDEX('${toFetch}') = SOUNDEX(name)`,
      { type: QueryTypes.SELECT }
    );

    if (res.length < 1) {
      ctx.channel.send("Ability not found. Check your spelling and try again.");
      return;
    }

    const ability: { [key: string]: any } = res[0];

    const embed = new MessageEmbed()
      .setTitle(ability.name)
      .setDescription(ability.desc || ability.short_desc)
      .addField(
        "Smogon Rating",
        `${this.ratings[Math.floor(ability.rating / 10)]} (${
          ability.rating / 10
        })`
      )
      .addField("External Resources", this.genExternal(ability));

    await ctx.channel.send({ embed });
  }

  genExternal(ability: any): string {
    const extStrings = [
      ["Bulbapedia", ability.name, "bulbapedia"],
      ["PokémonDB", ability.name, "pokemondb"],
      ["Smogon", ability.name, "smogon"],
      ["Serebii", ability.name, "serebii"],
    ].map((s) => `[${s[0]}](${this.external.generate(s[1], "ability", s[2])})`);
    return extStrings.join(" • ");
  }
}

export default AbilityCommand;
