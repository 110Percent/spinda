import Command from "../class/Command";
import {
  Message,
  MessageEmbed,
} from "discord.js";
import Spinda from "../class/Spinda";

class TopCommand extends Command {
  constructor(spinda: Spinda) {
    super(
      "top",
      {
        desc: "Shows the most commonly-referenced Pokémon in this server.",
        category: "Utility",
        guildRequired: true,
      },
      spinda
    );
  }

  async run(ctx: Message, args: string): Promise<void> {
    const amount =
      isNaN(Number(args)) || Number(args) < 1 || Number(args) > 10
        ? 5
        : Number(args);
    const records = await this.spinda.analytica.fetch(
      { identifier: <string>ctx.guild?.id, type: "GUILD" },
      amount
    );
    const embed = new MessageEmbed()
      .setTitle(`Top-searched Pokémon for guild ${ctx.guild?.name}`)
      .setThumbnail(ctx.guild?.iconURL() || "")
      .setDescription(
        records
          .map(
            (r: {name: string, hits: number}) =>
              `**${
                r.name.charAt(0).toUpperCase() + r.name.slice(1).toLowerCase()
              }** (${r.hits})`
          )
          .join("\n")
      );

    await ctx.channel.send({ embed });
  }
}

export default TopCommand;
