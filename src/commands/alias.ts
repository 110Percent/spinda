import Command from "../class/Command";
import {
  Message,
  Guild,
  MessageEmbed,
  MessageReaction,
  User,
} from "discord.js";
import Spinda from "../class/Spinda";

class AliasCommand extends Command {
  constructor(spinda: Spinda) {
    super(
      "alias",
      {
        desc: "Manages Pok\u00e9mon aliases for auto-spriting and commands.",
        usage: ["list", "set <alias> <value>", "remove <alias>"],
        category: "Utility",
        perms: ["MANAGE_GUILD"],
      },
      spinda
    );
  }

  async run(ctx: Message, args: string): Promise<void> {
    if (!args || !ctx.guild) return;
    const split = args.split(" ");
    if (split[0] === "list") {
      await this.subList(ctx);
    } else if (split[0] === "set") {
      const parsed = this.quoteParse(split.slice(1).join(" "));
      await this.subSet(ctx, parsed[0].toLowerCase(), parsed[1].toLowerCase());
    } else if (split[0] === "remove") {
      const parsed = this.quoteParse(split.slice(1).join(" "));
      await this.subRemove(ctx, parsed[0]);
    }
  }

  quoteParse(query: string): string[] {
    let parsed = query.match(/(?:[^\s"]+|"[^"]*")+/g);
    if (!parsed) {
      parsed = query.split(" ");
    }
    parsed = parsed.map((c) => c.replace(/"/g, ""));
    return parsed;
  }

  async subList(ctx: Message): Promise<void> {
    const entries = await this.spinda.aliasManager.list((<Guild>ctx.guild).id);
    const embed = new MessageEmbed({
      title: `Aliases for guild ${(<Guild>ctx.guild).name}`,
      description: entries
        .map((a: any) => `▫️ \`${a.key}\` → \`${a.value}\``)
        .join("\n"),
    });
    await ctx.channel.send({ embed });
  }

  async subSet(ctx: Message, key: string, value: string): Promise<void> {
    await this.spinda.aliasManager.set(key, value, (<Guild>ctx.guild).id);
    await ctx.channel.send(
      `Set alias \`${key}\` to \`${value}\` for this guild.`
    );
  }

  async subRemove(ctx: Message, key: string): Promise<void> {
    const entry = await this.spinda.aliasManager.check(
      key,
      (<Guild>ctx.guild).id
    );
    if (entry !== key) {
      const msg = await ctx.channel.send(
        `Remove alias \`${key}\` (\`${entry}\`)?`
      );
      await msg.react("👍");
      await msg.awaitReactions(
        (r: MessageReaction, u: User) =>
          r.emoji.name === "👍" && u.id === ctx.author.id,
        { max: 1 }
      );
      await this.spinda.aliasManager.delete(key, (<Guild>ctx.guild).id);
      await msg.edit(`Deleted alias \`${key}\` for this guild.`);
    } else {
      await ctx.channel.send(`Alias \`${key}\` not found.`);
    }
  }
}

export default AliasCommand;
