import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import { QueryTypes } from "sequelize";
import ExternalGenerator from "../class/ExternalGenerator";

class ItemCommand extends Command {
  external: ExternalGenerator;

  constructor(spinda: Spinda) {
    super(
      "item",
      {
        desc: "Shows information on an item.",
        category: "Pokémon",
        usage: ["<item>"],
      },
      spinda
    );

    this.external = new ExternalGenerator();
  }

  async run(ctx: Message, args: string): Promise<void> {
    const toFetch = args.replace(/[^a-zA-Z0-9]/g, "");

    if (toFetch.length < 1) {
      ctx.channel.send("Item not found. Check your spelling and try again.");
      return;
    }

    const res = await this.spinda.minccino.query(
      `SELECT * FROM \`Items\` where SOUNDEX('${toFetch}') = SOUNDEX(name)`,
      { type: QueryTypes.SELECT }
    );

    if (res.length < 1) {
      ctx.channel.send("Item not found. Check your spelling and try again.");
      return;
    }

    const item: { [key: string]: any } = res[0];
    const embed = new MessageEmbed()
      .setTitle(item.name)
      .setDescription(
        (item.desc ? `*${item.desc}*\n\n` : "") + (item.comp_desc || "")
      )
      .addField("External Resources", this.genExternal(item));

    await ctx.channel.send({ embed });
  }

  genExternal(item: any): string {
    const extStrings = [
      ["Bulbapedia", item.name, "bulbapedia"],
      ["PokémonDB", item.name, "pokemondb"],
      ["Smogon", item.name, "smogon"],
      ["Serebii", item.name, "serebii"],
    ].map((s) => `[${s[0]}](${this.external.generate(s[1], "item", s[2])})`);
    return extStrings.join(" • ");
  }
}

export default ItemCommand;
