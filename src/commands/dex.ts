import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import sequelize, { QueryTypes } from "sequelize";
import { Colors } from "../types/Colors";
import ExternalGenerator from "../class/ExternalGenerator";

class DexCommand extends Command {
  pokeTypes: string[];
  pokeAbilities: any[];
  eggGroups: string[];
  stats: string[];
  prettyNames: { [key: string]: string };
  spriteFormes: { [key: string]: string };
  external: ExternalGenerator;

  constructor(spinda: Spinda, light?: boolean) {
    super(
      light ? "ldex" : "dex",
      {
        desc: light
          ? "Shows a lighter dex page for a Pokémon."
          : "Shows information on a Pokémon.",
        category: "Pokémon",
        usage: ["<pokemon>"],
      },
      spinda
    );
    this.pokeTypes = [];
    this.pokeAbilities = [];
    this.eggGroups = [];
    this.stats = ["HP", "Atk", "Def", "SpA", "SpD", "Spe"];
    this.prettyNames = {
      "(.*)-Gmax": "Gigantamax &",
      "(.*)-Mega$": "Mega &",
      "(.*)-Mega-X": "Mega & X",
      "(.*)-Mega-Y": "Mega & Y",
      "(.*)-Zen": "& (Zen Mode)",
      "(.*)-Alola": "Alolan &",
      "(.*)-Galar": "Galarian &",
      "(.*)-Sunny": "& (Sunny Forme)",
      "(.*)-Rainy": "& (Rainy Forme)",
      "(.*)-Snowy": "& (Snowy Forme)",
      "(.*)-Primal": "Primal &",
      "(.*)-Attack": "& (Attack Forme)",
      "(.*)-Defense": "& (Defense Forme)",
      "(.*)-Speed": "& (Speed Forme)",
      "(.*)-Sandy": "& (Sandy Cloak)",
      "(.*)-Trash": "& (Trash Cloak)",
      "(.*)-Sunshine": "& (Sunshine Forme)",
      "(.*)-Heat": "Heat &",
      "(.*)-Wash": "Wash &",
      "(.*)-Frost": "Frost &",
      "(.*)-Fan": "Fan &",
      "(.*)-Mow": "Mow &",
      "(.*)-Origin": "& (Origin Forme)",
      "(.*)-Sky": "& (Sky Forme)",
      "(.*)-Therian": "& (Therian Forme)",
      "(.*)-White": "White &",
      "(.*)-Black": "Black &",
      "(.*)-Resolute": "& (Resolute Forme)",
      "(.*)-Pirouette": "& (Pirouette Forme)",
      "(.*)-Ash": "Ash &",
      "(.*)-Blade": "& (Blade Forme)",
      "(.*)-Small": "Small &",
      "(.*)-Large": "Large &",
      "(.*)-Super": "Super Size &",
      "(.*)-10%": "& 10%",
      "(.*)-Complete": "& Complete",
      "(.*)-Unbound": "& Unbound",
      "(.*)-Pom-Pom": "& Pom-Pom Style",
      "(.*)-Pa'u": "& Pa'u Style",
      "(.*)-Sensu": "& Sensu Style",
      "(.*)-Midnight": "& Midnight Forme",
      "(.*)-Dusk": "& Dusk Forme",
      "(.*)-School": "& School Forme",
      "(.*)-Meteor": "& Meteor Forme",
      "(.*)-Busted": "& Busted Forme",
      "(.*)-Totem": "Totem &",
      "(.*)-Dusk-Mane": "Dusk-Mane &",
      "(.*)-Dawn-Wings": "Dawn-Wings &",
      "(.*)-Ultra": "Ultra &",
      "(.*)-Gulping": "& (Gulping Forme)",
      "(.*)-Gorging": "& (Gorging Forme)",
      "(.*)-Low-Key": "& (Low Key Forme)",
      "(.*)-Antique": "& (Antique Forme)",
      "(.*)-Noice": "& (Noice Face)",
      "(.*)-Hangry": "& (Hangry Mode)",
      "Zacian-Crowned": "Zacian Crowned Sword",
      "Zamazenta-Crowned": "Zamazenta Crowned Shield",
      "(.*)-Eternamax": "& Eternamax",
      "(.*)-Rapid-Strike": "& Rapid Strike Style",
    };
    this.spriteFormes = {
      "-mega": "-mega",
      "-mega-x": "-megax",
      "-mega-y": "-megay",
      "-gmax": "-gmax",
      "-alola": "-ra",
      "-galar": "-rg",
      "-zen": "-fzen",
      "-sunny": "-fsunny",
      "-rainy": "-frainy",
      "-snowy": "-fsnowy",
      "-primal": "-fprimal",
      "-attack": "-fatk",
      "-defense": "-fdef",
      "-speed": "-fspe",
      "-sandy": "-fsand",
      "-trash": "-ftrash",
      "-sunshine": "-fsun",
      "-heat": "-fheat",
      "-wash": "-fwash",
      "-frost": "-ffrost",
      "-fan": "-ffan",
      "-mow": "-fmow",
      "-origin": "-forigin",
      "-sky": "-fsky",
      "-therian": "-ftherian",
      "-white": "-fwhite",
      "-black": "-fblack",
      "-resolute": "-fres",
      "-pirouette": "-fpir",
      "-ash": "-fash",
      "-blade": "-fblade",
      "-small": "-fsmall",
      "-large": "-flarge",
      "-super": "-fsuper",
      "-10%": "-f10",
      "-complete": "-fcomplete",
      "-unbound": "-fun",
      "-pom-pom": "-fpp",
      "-pa'u": "-fpau",
      "-sensu": "-fsensu",
      "-midnight": "-fnight",
      "-dusk": "-fdusk",
      "-school": "-fschool",
      "-meteor": "-fmeteor",
      "-busted": "-fbust",
      "-dusk-mane": "-fdusk",
      "-dawn-wings": "-fdawn",
      "-ultra": "-fult",
      "-gulping": "-fgulp",
      "-gorging": "-fgorge",
      "-low-key": "-flow",
      "-antique": "-fant",
      "-noice": "-fnoice",
      "-hangry": "-fhangry",
      "-crowned": "-fcrown",
      "-eternamax": "-femax",
    };
    this.setIndices();
    this.external = new ExternalGenerator();
  }

  async run(ctx: Message, args: string): Promise<void> {
    const search = ctx.guild
      ? await this.spinda.aliasManager.check(args, ctx.guild.id)
      : args;
    const pokemon: { [key: string]: any } | null = await this.searchPokemon(
      this.formatQuery(search)
    );

    if (!pokemon) {
      ctx.channel.send(
        "Pok\u00e9mon not found. Check your spelling and try again."
      );
      return;
    }

    const types = await this.getTypes(pokemon);
    const abilities = await this.findAbilities(pokemon);
    const stats = await this.genStats(pokemon);

    const groups: string[] = [this.eggGroups[pokemon.egg_1 - 1]];
    if (pokemon.egg_2 !== null) {
      groups.push(this.eggGroups[pokemon.egg_2 - 1]);
    }

    const basePrevo = await this.prevoScan(pokemon.name);
    const evoTree = await this.evoTree(basePrevo);
    const evoList = this.evoSort(evoTree);
    const evoString = evoList
      .map((l: string[]) => {
        return l
          .map((p: string) => {
            return p === pokemon.name
              ? `**${this.prettyName(p)}**`
              : this.prettyName(p);
          })
          .join(", ");
      })
      .join(" → ");

    const color: any = await this.spinda.minccino.models["Colors"].findOne({
      where: { id: pokemon.color },
      raw: true,
    });
    const colorString = color.name;

    const prettyName = this.prettyName(pokemon.name);
    const embed = new MessageEmbed()
      .setTitle(
        `${prettyName} ${
          pokemon.nonstandard === "Past" ? "" : "<:SwSh:731188102273433600>"
        }`
      )
      .setColor(Colors[colorString.toUpperCase()])
      .addField(types.length > 1 ? "Types" : "Type", types.join(", "), true)
      .addField(
        abilities.length > 1 ? "Abilities" : "Ability",
        abilities.join(", "),
        true
      );
    if (pokemon.dex_entry) {
      embed.setDescription(`*${pokemon.dex_entry}*`);
    }
    if (evoList.length > 1) {
      embed.addField("Evolution", evoString);
    }
    embed
      .addField("Base Stats", stats.join(", "))
      .addField("Height", `${pokemon.height_m} m`, true)
      .addField("Weight", `${pokemon.weight_kg} kg`, true)
      .addField("Smogon Tier", pokemon.tier, true)
      .addField(
        "Egg " + (groups.length > 1 ? "Groups" : "Group"),
        groups.join(", ")
      )
      .addField("External Resources", this.genExternal(pokemon))
      .setThumbnail(this.spriteUrl(pokemon));

    await ctx.channel.send({ embed });
    this.spinda.analytica.logPokemon(ctx, pokemon.name);
  }

  async setIndices(): Promise<void> {
    this.pokeTypes = (
      await this.spinda.minccino.models["PokeTypes"].findAll({ raw: true })
    ).map((t: any) => t.name);
    this.pokeAbilities = await this.spinda.minccino.models[
      "Abilities"
    ].findAll({ raw: true });
    this.eggGroups = (
      await this.spinda.minccino.models["EggGroups"].findAll({ raw: true })
    ).map((g: any) => g.name);
  }

  async searchPokemon(
    toFetch: string,
    rev?: boolean
  ): Promise<{ [key: string]: any } | null> {
    if (toFetch.length < 1) {
      return null;
    }

    let res = await this.spinda.minccino.models["Pokemon"].findAll({
      where: sequelize.where(
        sequelize.fn("lower", sequelize.col("name")),
        toFetch
      ),
    });

    if (res.length < 1) {
      res = await this.spinda.minccino.query(
        `SELECT * FROM \`Pokemon\` WHERE SOUNDEX('${toFetch}') = SOUNDEX(name)`,
        { type: QueryTypes.SELECT }
      );
    }

    if (res.length < 1) {
      if (!rev && toFetch.split("-").length === 2) {
        const reversed = toFetch.split("-").reverse().join("-");
        return this.searchPokemon(reversed, true);
      } else {
        return null;
      }
    }

    return res[0];
  }

  async getTypes(pokemon: { [key: string]: any }): Promise<string[]> {
    const types: string[] = [this.pokeTypes[pokemon.type1 - 1]];
    if (pokemon.type2 !== null) {
      types.push(this.pokeTypes[pokemon.type2 - 1]);
    }
    return types;
  }

  async findAbilities(pokemon: { [key: string]: any }): Promise<string[]> {
    const abilities = [
      this.pokeAbilities[
        this.pokeAbilities.findIndex((c) => c.num === pokemon.ability_1)
      ].name,
    ];
    if (pokemon.ability_2 !== null) {
      abilities.push(
        this.pokeAbilities[
          this.pokeAbilities.findIndex((c) => c.num === pokemon.ability_2)
        ].name
      );
    }
    if (pokemon.ability_h !== null) {
      abilities.push(
        `*${
          this.pokeAbilities[
            this.pokeAbilities.findIndex((c) => c.num === pokemon.ability_h)
          ].name
        }*`
      );
    }
    return abilities;
  }

  async genStats(pokemon: { [key: string]: any }): Promise<string[]> {
    const stats: string[] = [];
    let totalStats = 0;
    for (const stat of this.stats) {
      stats.push(`**${stat}**: ${pokemon[stat.toLowerCase()]}`);
      totalStats += pokemon[stat.toLowerCase()];
    }
    stats.push(`**Total**: ${totalStats}`);
    return stats;
  }

  async prevoScan(name: string): Promise<string> {
    const preEntry: any[] = await this.spinda.minccino.models[
      "EvoRelations"
    ].findAll({
      where: {
        evo: name,
      },
      raw: true,
    });
    return preEntry.length < 1 ? name : await this.prevoScan(preEntry[0].prevo);
  }

  async evoTree(name: string): Promise<any> {
    const evoEntries: any[] = await this.spinda.minccino.models[
      "EvoRelations"
    ].findAll({
      where: {
        prevo: name,
      },
      raw: true,
    });

    if (evoEntries.length < 1) {
      return { name };
    } else {
      return {
        name,
        evos: await Promise.all(
          evoEntries.map(async (c: any) => {
            return await this.evoTree(c.evo);
          })
        ),
      };
    }
  }

  evoSort(
    tree: { name: string; evos?: any },
    tNames?: string[][],
    tLayer?: number
  ): string[][] {
    const layer = tLayer || 0;
    let names = tNames || [];
    if (!names[layer]) {
      names[layer] = [];
    }
    names[layer].push(tree.name);
    if (tree.evos) {
      for (const evo of tree.evos) {
        names = this.evoSort(evo, names, layer + 1);
      }
    }
    return names;
  }

  spriteUrl(pokemon: { [key: string]: any }): string {
    let name = pokemon.name;
    let base = pokemon.species_id.toString().padStart(3, "0");
    for (const key of Object.keys(this.spriteFormes)) {
      if (name.toLowerCase().includes(key)) {
        base += this.spriteFormes[key];
        name = name.replace(key, "");
      }
    }
    return `http://spinda.b-cdn.net/sprites/home/${base}.png`;
  }

  prettyName(name: string): string {
    let pName = name;
    for (const key of Object.keys(this.prettyNames)) {
      const match = pName.match(new RegExp(key));
      if (match) {
        pName = pName.replace(
          match[0],
          this.prettyNames[key].replace("&", match[1])
        );
      }
    }
    return pName;
  }

  formatQuery(name: string): string {
    const split = name
      .toLowerCase()
      .replace(/[-]/g, " ")
      .replace(/[^\w\s%]/g, "")
      .split(" ");
    const addons = [];
    for (let i = 0; i < split.length; i++) {
      const word = split[i];
      if (["form", "forme", "style"].indexOf(word) > -1) {
        split.splice(split.indexOf(word), 1);
        i--;
      } else if (["gigantamax", "gmax"].indexOf(word) > -1) {
        split.splice(split.indexOf(word), 1);
        i--;
        addons.push("gmax");
      } else if (word === "mega") {
        split.splice(split.indexOf(word), 1);
        i--;
        addons.push("mega");
      } else if (["x", "y"].indexOf(word) > -1 && addons.indexOf("mega") > -1) {
        split.splice(split.indexOf(word), 1);
        i--;
        addons.push(word);
      } else if (word.includes("galar")) {
        split.splice(split.indexOf(word), 1);
        i--;
        addons.push("galar");
      } else if (word.includes("alola")) {
        split.splice(split.indexOf(word), 1);
        i--;
        addons.push("alola");
      }
    }
    for (const addon of addons) {
      split.push(addon);
    }
    return split.join("-");
  }

  genExternal(pokemon: any) {
    const prettyName = this.prettyName(pokemon.name);
    const extStrings = [
      ["Bulbapedia", prettyName, "bulbapedia"],
      ["PokémonDB", prettyName, "pokemondb"],
      ["Smogon", pokemon.name, "smogon"],
      ["Serebii", pokemon.species_id, "serebii"],
    ].map((s) => `[${s[0]}](${this.external.generate(s[1], "pokemon", s[2])})`);
    return extStrings.join(" • ");
  }
}

export default DexCommand;
