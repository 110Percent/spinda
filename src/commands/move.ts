import Command from "../class/Command";
import { Message, MessageEmbed } from "discord.js";
import Spinda from "../class/Spinda";
import { QueryTypes } from "sequelize";
import sequelize from "sequelize";
import ExternalGenerator from "../class/ExternalGenerator";
import { TypeColors } from "../types/Colors";

class MoveCommand extends Command {
  pokeTypes: string[];
  targets: string[];
  external: ExternalGenerator;
  categoryEmojis: { [key: string]: string };

  constructor(spinda: Spinda) {
    super(
      "move",
      {
        desc: "Shows information on a move.",
        category: "Pokémon",
        usage: ["<move>"],
      },
      spinda
    );
    this.pokeTypes = [];
    this.targets = [];
    this.categoryEmojis = {
      Physical: "<:physical:731138469472108665>",
      Special: "<:special:731138469920768080>",
      Status: "<:status:731138469631492136>",
    };
    this.spinda.minccino.models["PokeTypes"]
      .findAll({ raw: true })
      .then((ts: any[]) => {
        this.pokeTypes = ts.map((t) => t.name);
      });
    this.spinda.minccino.models["MoveTargets"]
      .findAll({ raw: true })
      .then((ts: any[]) => {
        this.targets = ts.map((t) => t.name);
      });
    this.external = new ExternalGenerator();
  }

  async run(ctx: Message, args: string): Promise<void> {
    const move: { [key: string]: any } | null = await this.searchMove(args);

    if (!move) {
      ctx.channel.send("Move not found. Check your spelling and try again.");
      return;
    }

    const embed = new MessageEmbed()
      .setColor((<any>TypeColors)[this.pokeTypes[move.move_type - 1]])
      .setTitle(`${this.categoryEmojis[move.category]} ${move.name}`)
      .setDescription(
        (move.desc ? `*${move.desc}*\n\n` : "") + (move.comp_desc || "")
      )
      .addField("Type", this.pokeTypes[move.move_type - 1], true)
      .addField("PP", move.pp, true);

    if (move.base_power > 0) {
      embed.addField("Base Power", move.base_power, true);
    }

    embed
      .addField(
        "Accuracy",
        move.accuracy === 1 ? "—" : `${move.accuracy}%`,
        true
      )
      .addField("Target", this.targets[move.target - 1], true)
      .addField("Priority", move.priority, true)
      .addField("External Resources", this.genExternal(move.name));

    await ctx.channel.send({ embed });
  }

  async searchMove(search: string): Promise<{ [key: string]: any } | null> {
    const toFetch = search
      .toLowerCase()
      .replace(/\s/g, "-")
      .replace(/[^\w\-%]/g, "");

    if (toFetch.length < 1) {
      return null;
    }

    let res = await this.spinda.minccino.models["Moves"].findAll({
      where: sequelize.where(
        sequelize.fn("lower", sequelize.col("name")),
        toFetch
      ),
    });

    if (res.length < 1) {
      res = await this.spinda.minccino.query(
        `SELECT * FROM \`Moves\` WHERE SOUNDEX('${toFetch}') = SOUNDEX(name)`,
        { type: QueryTypes.SELECT }
      );

      if (res.length < 1) {
        return null;
      }
    }

    return res[0];
  }

  genExternal(move: string): string {
    const extStrings = [
      ["Bulbapedia", move, "bulbapedia"],
      ["PokémonDB", move, "pokemondb"],
      ["Smogon", move, "smogon"],
      ["Serebii", move, "serebii"],
    ].map((s) => `[${s[0]}](${this.external.generate(s[1], "move", s[2])})`);
    return extStrings.join(" • ");
  }
}

export default MoveCommand;
