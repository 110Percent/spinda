import Command from "../class/Command";
import { Message, MessageEmbed, MessageEmbedOptions } from "discord.js";
import Spinda from "../class/Spinda";
import request from "superagent";

class TCGCommand extends Command {
  endpoint: string;
  methods: { [key: string]: any };

  constructor(spinda: Spinda) {
    super(
      "tcg",
      {
        desc: "Utilities related to the Pokémon TCG",
        category: "Pokémon",
        aliases: ["card"],
      },
      spinda
    );

    this.endpoint = "https://api.pokemontcg.io/v1/cards";
    this.methods = { search: this.search, show: this.show };
  }

  async run(ctx: Message, args: string): Promise<void> {
    const split = args.split(" ");
    if (split.length < 2) return;

    const sub = split.splice(0, 1)[0];
    if (this.methods[sub]) {
      const msg = await ctx.channel.send("Querying; please wait...");
      const embed = await this.methods[sub].bind(this)(ctx, split.join(" "));
      msg.edit("", embed);
    }
  }

  async search(ctx: Message, query: string): Promise<MessageEmbed> {
    const res = await request
      .get(this.endpoint)
      .send({ name: query })
      .set("Accept", "application/json");
    const cards = res.body.cards;

    let embed: MessageEmbedOptions = { title: `Results for "${query}"` };
    if (cards.length > 0) {
      embed = {
        ...embed,
        ...{
          description: `[Click me](https://pokemontcg.io/cards?name=${encodeURIComponent(
            query
          )}) for a full list of cards.`,
          fields: cards.slice(0, 9).map((c: any) => {
            return {
              name: c.supertype,
              value: `${c.name}\n*${c.set} (${c.setCode})*`,
              inline: true,
            };
          }),
          thumbnail: { url: cards[0].imageUrl },
        },
      };
    } else {
      embed.description = "No results found.";
    }

    return new MessageEmbed(embed);
  }

  async show(ctx: Message, query: string): Promise<MessageEmbed> {
    const split = query.split(" ");
    let embed: MessageEmbedOptions = {};
    if (split.length < 2) {
      embed.description =
        "Invalid format. Please use `<set code>` `<card name>.";
    } else {
      const setCode = split.splice(0, 1)[0];
      const res = await request
        .get(this.endpoint)
        .send({ setCode, name: split.join(" ") })
        .set("Accept", "application/json");
      const cards = res.body.cards;

      if (cards.length > 0) {
        embed = {
          title: cards[0].name,
          description: `[More info](https://pokemontcg.io/cards/${cards[0].id})`,
          fields: [
            { name: "Set", value: `${cards[0].set} (${cards[0].setCode})` },
            { name: "Artist", value: cards[0].artist, inline: true },
            { name: "Rarity", value: cards[0].rarity, inline: true },
          ],
          image: { url: cards[0].imageUrlHiRes || cards[0].imageUrl },
        };
      }
    }
    return new MessageEmbed(embed);
  }
}

export default TCGCommand;
