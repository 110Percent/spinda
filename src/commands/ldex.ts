import DexCommand from "./dex";
import Spinda from "../class/Spinda";
import { Message, MessageEmbed } from "discord.js";
import { Colors } from "../types/Colors";

class LightDexCommand extends DexCommand {
  constructor(spinda: Spinda) {
    super(spinda, true);
  }

  async run(ctx: Message, args: string): Promise<void> {
    const pokemon: { [key: string]: any } | null = await this.searchPokemon(
      args
    );

    if (!pokemon) {
      ctx.channel.send(
        "Pok\u00e9mon not found. Check your spelling and try again."
      );
      return;
    }

    const types = await this.getTypes(pokemon);

    const abilities = await this.findAbilities(pokemon);

    const stats = await this.genStats(pokemon);

    const color: any = await this.spinda.minccino.models["Colors"].findOne({
      where: { id: pokemon.color },
      raw: true,
    });
    const colorString = color.name;

    const embed = new MessageEmbed()
      .setTitle(pokemon.name)
      .setColor(Colors[colorString.toUpperCase()])
      .addField(types.length > 1 ? "Types" : "Type", types.join(", "), true)
      .addField(
        abilities.length > 1 ? "Abilities" : "Ability",
        abilities.join(", "),
        true
      )
      .addField("Base Stats", stats.join(", "))
      .setThumbnail(this.spriteUrl(pokemon));

    await ctx.channel.send({ embed });
    this.spinda.analytica.logPokemon(ctx, pokemon.name);
  }
}

export default LightDexCommand;
